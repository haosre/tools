#!/bin/bash

# Create by: hao.nguyen
# Last modify: 2021-03-01

sudo mkdir -p /opt/bootstrap

sudo wget https://github.com/prometheus/node_exporter/releases/download/v1.1.1/node_exporter-1.1.1.linux-amd64.tar.gz -P /opt/bootstrap
sudo tar -C /opt/bootstrap -xzf /opt/bootstrap/node_exporter-1.1.1.linux-amd64.tar.gz
sudo cp /opt/bootstrap/node_exporter-1.1.1.linux-amd64/node_exporter /usr/local/bin
sudo chmod 755 /usr/local/bin/node_exporter

sudo useradd --no-create-home --shell /bin/false node-exporter

sudo /bin/bash -c 'cat <<EOF > /etc/systemd/system/node-exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node-exporter
Group=node-exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF'

sudo /usr/bin/systemctl daemon-reload
sudo /usr/bin/systemctl enable node-exporter
sudo /usr/bin/systemctl start node-exporter
